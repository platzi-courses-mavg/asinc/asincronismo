const fetchData = require('../utils/fetchData');
let API = 'https://rickandmortyapi.com/api/character/'

fetchData(API)
  .then(data => {
    console.log(data.info.count);
    return fetchData(`${API}${data.results[0].id}`) // Uso de Templay literals
  })
  .then(data => {
    console.log(data.name);
    return fetchData(data.origin.url)
  })
  .then(data => {
    console.log(data.dimension);
  })
  .catch(err => console.log(err));