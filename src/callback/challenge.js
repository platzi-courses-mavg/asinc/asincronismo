let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
let API = 'https://rickandmortyapi.com/api/character/'

function fetchData(url_api, callback) {
  /* XMLHttpRequest, también referida como XMLHTTP, es una interfaz empleada para realizar peticiones HTTP y HTTPS a servidores Web. 
  Para los datos transferidos se usa cualquier codificación basada en texto, incluyendo: texto plano, XML, JSON, HTML y codificaciones particulares específicas. */
  let xhttp = new XMLHttpRequest();
  xhttp.open('GET', url_api, true) // El tercer valor hace referencia a la activación del asincronismo dentro de XMLHttpRequest
  xhttp.onreadystatechange = function (event) {
    /* Espacio que permite hacer validación para saber si voy a ejecutar el callback.
      La validación consiste en revisar si el estado en el cual se encuentra es 
      satisfactorio. Existen cinco diferentes estados (readyState): 
        0.- Solicitud no incializada.
        1.- Conexión al servidor establecida.
        2.- Solicitud recibida.
        3.- Procesando solicitud.
        4.- Solicitud finalizada y respuesta lista.
          (https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp)
    */
    if (xhttp.readyState === 4)
      if (xhttp.status === 200) {
        callback(null, JSON.parse(xhttp.responseText))
      } else {
        const error = new Error('Error ' + url_api);
        return callback(error, null)
      }
  }
  xhttp.send()
}

fetchData(API, function (error1, data1) {
  if (error1) return console.error(error1);
  fetchData(API + data1.results[0].id, function (error2, data2) { // .results es una clave valor de la respuesta a API
    if (error2) return console.error(error2);
    fetchData(data2.origin.url, function (error3, data3) { // .origin.url es un elemento de la respuesta de la API
      if (error3) return console.error(error3);
      console.log(data1.info.count);
      console.log(data2.name);
      console.log(data3.dimension);      
    }) 
  }) 
})

// NOTA: SE RECOMIENDA NO HACER UN CALLBACK HELL DE MAS DE TRES PETICIONES