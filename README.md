# Asíncronismo <a name="inicio"/>

## Contenido
* 1. [¿Qué es el asíncronismo en JavaScript?](#Asíncronismo)
* 2. [Callbacks](#Callbacks)
* 3. [Promises](#Promises)
* 4. [Async-Await](#Async-Await)


---
## ¿Qué es el asíncronismo en JavaScript? <a name="Asíncronismo"/> [↑](#inicio)
Javascript es un lenguaje de programación asíncrono y no bloqueante con un manejador de eventos, conocido como **Event Loop**, implementado en único hilo para sus interfaces de entrada y salida.

**Asíncronismo.** Es la acción que no ocurre al mismo tiempo. [+](https://lemoncode.net/lemoncode-blog/2018/1/29/javascript-asincrono)

### ¿Cómo funciona JavaScript con el asíncronismo?
**Memory heap.** Es el espacio en memoria compartido para toda nuestra aplicación. 

**Pila de Ejecución (Call Stack).** Donde nuestras funciones son puestas en ejecución.

*Ciclo de vida que tienen las implementaciones.* Cierta tarea o función tiene un llamado a un setTimeOut o a una API, la cual se desencandena por medio de un **Callback**, el cuál es puesto en nuestra **Cola de Tareas**. Mientras tanto nosotros podemos tener otras funciones/tareas que se estarán ejecutando.
El **Event loop ([+](https://medium.com/@ubykuo/event-loop-la-naturaleza-asincr%C3%B3nica-de-javascript-78d0a9a3e03d))** es el encargado de preguntar o entender si la Pila de Ejecución esta vacía para entonces resolver una nueva función (el llamado de una API o un setTimeOut) haciendo un Callback de la misma.

**Callback** es una función que es pasada como parámetro a otra función; mientras que **Función de Orden Superior (Higher-order Function)** es una función que recibe como parámetro a otra función. Al momento de hacer una petición o algún llamado asíncrono esta función Callback, se ejecuta después de su llamado.

**NOTAS:**
* Es recomendado no hacer funciones que puedan ser muy pesadas, es decir, que puedan hacer bastantes llamados a apis o procesamientos que puedan estar saturando nuestra *pila de ejecución*.
* Se recomienda no hacer un *callback hell* (anidación de callbacks) de más de tres peticiones.
* [APIs Públicas](https://github.com/public-apis/public-apis)
* [Rick & Morty API](https://rickandmortyapi.com/)


---
## Callbacks <a name="Callbacks"/> [↑](#inicio)
Una función de callback es una función que se pasa a otra función como un argumento, que luego se invoca dentro de la función externa para completar algún tipo de rutina o acción.

**Ventajas:**
* Son simples de utilizar. Simplemente es una función que se envía a otra función.
* Son universales, esto significa que pueden correr en cualquier navegador.

**Desventajas:**
* La composición suele ser tozca, anidando elementos entre si después de hacer una nueva petición, llegando al *callback hell*.
* Tiene un flujo poco intuitivo.

[Código](./src/callback)


---
## Promises <a name="Promises"/> [↑](#inicio)
Una promesa representa un valor que puede estar disponible ahora, en el futuro, o nunca.

**Ventajas:**
* Facilmente enlazables, ayuda a tener una gran cantidad de llamadas de manera mas entendible.
* Permiten gran capacidad de trabajar con asíncronismo.

**Desventajas:**
* No maneja excepciones, solo se cuenta con un catch al final.
* Propensas a errores si no se retorna el siguiente llamado.
* Requiere un polyfill* para funcionar en todos los navergadores, es decir, es necesario transpilar el código con herramientas como Babel.

  \* *Un polyfill es un fragmento de código (generalmente JavaScript en la Web) que se utiliza para proporcionar una funcionalidad moderna en navegadores antiguos que no lo admiten de forma nativa.*

[Código](./src/promise)


---
## Async Await <a name="Async-Await"/> [↑](#inicio)
Async define una función asíncrona, la cual devuelve un objeto AsyncFunction. Es posible definir también funciones asíncronas a través de una expresión de función async.

Nace dentro de ECMAScript y es preferible ante las promesas ya que hacen que nuestro código se comporte como síncrono.

**Ventajas:**
* Permite utilizar try/catch.
* Son más fáciles de leer, tienen un mejor entendimiento.

**Desventajas:**
* Debemos esperar en cada uno de los llamados
* Requiere un polyfill para funcionar en todos los navergadores, es decir, es necesario transpilar el código con herramientas como Babel.

[Código](./src/async)