const somethingWillHappen = () => {
  return new Promise((resolve, reject) => {
    if (true) {
    // if (false) {
      resolve('Hey!');
    } else {
      reject('Whoops!');
    }
  });
};

somethingWillHappen()
  .then(response => console.log(response))
  .catch(error => console.error(error));
  // .finally(console.log('Practicing finally'));



const somethingWillHappen2 = () => {
  return new Promise((resolve, reject) => {
    if (true) {
    // if (false) {
      setTimeout(() => {
        resolve('True');
      }, 2000)
    } else {
      const error = new Error('Whoops!');
      reject(error);
    }
  });
};

somethingWillHappen2()
  .then(response => console.log(response))
  // .then(() => console.log("Hi"))
  .catch(error => console.error(error))


Promise.all([somethingWillHappen(), somethingWillHappen2()])
  .then(response => {
    console.log('Array of results', response);
  })
  .catch(err => {
    console.error(err);
  })